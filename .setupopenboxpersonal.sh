#!/bin/bash

yay -S tint2 openbox obconf obmenu obkey-gtk3 lxappearance-gtk3 obmenu-generator wpgtk-git termite perl gtk2-perl perl-gtk3

# Applications
yay -S --needed firefox chromium steam rambox-bin unityhub libreoffice-still kdenlive audacity vlc playonlinux gimp krita emacs aspell-en libmythes mythes-en languagetool ttf-ms-fonts texlive-most gufw xf86-input-wacom git gnupg pass qtpass transmission-gtk wacom-utility

# sudo sed -i 's/GRUB_TIMEOUT=10/GRUB_TIMEOUT=0/g' /etc/default/grub && sudo update-grub

sudo echo "vm.swappiness=10" >> /etc/sysctl.d/99-sysctl.conf

# Or if you use GNOME or XFCE:

sudo echo "gksudo python3 /usr/lib/python3.7/site-packages/gufw/gufw.py" >> /bin/gufw

# Now, open GUFW from the main menu and turn it on.

# For most users just enabling the Firewall is enough.
sudo ufw enable
sudo systemctl enable ufw

sudo systemctl enable fstrim.timer

# git clone --depth 1 https://github.com/hlissner/doom-emacs ~/.emacs.d

# ~/.emacs.d/bin/doom install




mkdir ~/.icons
mkdir ~/.themes
mkdir ~/.config/openbox
cp /etc/xdg/openbox/* ~/.config/openbox

obmenu-generator -i -p

wpg-install.sh -toig
